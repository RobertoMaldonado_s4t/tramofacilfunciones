const crypto = require('crypto');

/*
if (process.env.NODE_ENV != "production") {
    require("dotenv").config();
}
*/

function getAlgorithm(keyBase64) {

    var key = Buffer.from(keyBase64, 'base64');
    switch (key.length) {
        case 16:
            return 'aes-128-cbc';
        case 32:
            return 'aes-256-cbc';
    }
    throw new Error('Invalid key length: ' + key.length);
}

function decrypt(messagebase64, keyBase64, ivBase64) {

    try {

        var key = Buffer.from(keyBase64, 'base64');
        var iv = Buffer.from(ivBase64, 'base64');
        const decipher = crypto.createDecipheriv(getAlgorithm(keyBase64), key, iv)
        return decipher.update(messagebase64, 'base64', 'utf8') + decipher.final('utf8')

    } catch (err) {

        return err;

    }

}

function getDecodificar(value) {

    if (value == null) {
        return '';
    }

    var buff = new Buffer(value, 'base64');
    var text = buff.toString('ascii');

    return text;
}

function desencriptarInformacion(parametros) {

    const isValidMessage = parametros;

    if (!isValidMessage) {

        var respuesta = {
            error: true,
            usuario: null
        }

        return respuesta;
    }

    var keyBase64 = "QmFyMTIzNDVCYXIxMjM0NQ==";
    var ivBase64 = "UmFuZG9tSW5pdFZlY3Rvcg==";

    var parametroDes = decrypt(getDecodificar(parametros), keyBase64, ivBase64);

    if (typeof(parametroDes) === 'object') {

        var respuesta = {
            error: true,
            usuario: null
        };

        return respuesta;
    }

    var usuario = JSON.parse(parametroDes);

    var respuesta = {
        error: false,
        usuario: usuario
    };

    return respuesta;
}

module.exports = {
    desencriptarInformacion
}