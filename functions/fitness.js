'use strict';
var createAPIRequest = require('apirequest');
const http = require('http');
var unirest = require('unirest');

function Fitness(options) {

    console.log("Fitness");

    var self = this;
    this._options = options || {};
    this.users = {
        dataSources: { // You have property 'dataSources' in users object that will be accessible via Fitness object(Publically)
            get: function(params, callback) {
                var parameters = {
                    options: {
                        url: 'http://httpbin.org/ip',
                        method: 'GET'
                    },
                    params: params,
                    requiredParams: ['userId', 'dataSourceId'],
                    pathParams: ['dataSourceId', 'userId'],
                    context: self
                };
                return createAPIRequest(parameters, callback);
            }
        }
    };
}


function getData() {
    var str = '';

    var options = {
        host: 'www.random.org',
        path: '/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'
    };

    var callback = function(response) {

        response.on('data', function(chunk) {
            str += chunk;
        });

        response.on('end', function() {
            console.log(req.data);
            console.log(str);
            // your code here if you want to use the results !
        });
    }

    var req = http.request(options, callback).end();
    console.log(req);
}


module.exports = {
    "getData": getData

}


var req = unirest('POST', 'http://gmandiola-eval-test.apigee.net/listarempresasporrutafiliado/obtener')
    .headers({
        'Content-Type': 'application/json'
    })
    .send(JSON.stringify({ "rutAfiliado": 12121 }))
    .end(function(res) {
        if (res.error) throw new Error(res.error);
        console.log(res.raw_body);
    });

//module.exports = Fitness;