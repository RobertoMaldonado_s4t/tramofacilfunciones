/********************* Obtener Token *********************/
var urlObtenerToken = {
    url: 'https://gmandiola-eval-prod.apigee.net/oauth/client_credential/accesstoken?grant_type=client_credentials',
    client_id: 'zoJ5plABodZGdAKgA81wjXXVoKUHFMdW',
    client_secret: 'HzkOtnrQCpiF43Da'
};


/********************* obtUsers *********************/
var urlObtUsers = {
    url: 'https://reqres.in/api/users'
};


/********************* obtAnios *********************/
var urlObtAnios = {
    url: 'https://gmandiola-eval-test.apigee.net/listaranios/obtener'
};


/********************* obtRazonSocial *********************/
var urlObtRazonSocial = {
    url: 'https://gmandiola-eval-test.apigee.net/obtenerrazonsocial/obtenerrazonsocial'
};


/********************* obtEmpresaPorRut *********************/
var urlObtEmpresaPorRut = {
    url: 'https://gmandiola-eval-test.apigee.net/listarempresaporrut/obtener'
};


/********************* obtEmpresaPorRutEmpresa *********************/
var urlObtEmpresaPorRutEmpresa = {
    url: 'https://desproducerws.ccaf.andes//WS_ListarEmpresasByPer_Rut/ListarEmpresasByPer_Rut'
};


/********************* obtEstadoDeclaracion *********************/
var urlObtEstadoDeclaracion = {
    url: 'http://gmandiola-eval-test.apigee.net/obtenerestadodeclaracion/obtener'
};


/********************* obtDetalleRenta *********************/
var urlObtDetalleRenta = {
    url: 'http://gmandiola-eval-prod.apigee.net/obtenerrentastramorut/obtener'
};


/********************* actualizarRenta *********************/
var urlActualizarRenta = {
    url: 'http://gmandiola-eval-test.apigee.net/actualizarrentasporrut/actualizar'
};

/********************* credencialesCorreo *********************/
var credencialesCorreo = {
    user: 'noresponderccla@gmail.com',
    pass: 's4t32020',
    service: 'gmail'
};

/********************* imagenCorreo *********************/
var imagenCorreo = {
    name: 'img-recuperar-clave-email.jpg',
    root: './images/img-recuperar-clave-email.jpg'
};

/********************* logoCorreo *********************/
var logoCorreo = {
    name: 'CCLA_logo.png',
    root: './images/CCLA_logo.png'
};


/********************* credenciales firebse *********************/
var credencialesFireBase = {
    apiKey: 'AIzaSyDzzYxlamyHohZsRVQXfqnTTqleh4r1gow',
    authDomain: 'buzoncla-dev.firebaseapp.com',
    projectId: 'buzoncla-dev'
};



/********************* exports *********************/
module.exports = {

    urlObtenerToken,
    urlObtUsers,
    urlObtAnios,
    urlObtRazonSocial,
    urlObtEmpresaPorRut,
    urlObtEmpresaPorRutEmpresa,
    urlObtEstadoDeclaracion,
    urlObtDetalleRenta,
    urlActualizarRenta,
    credencialesCorreo,
    imagenCorreo,
    logoCorreo,
    credencialesFireBase

}