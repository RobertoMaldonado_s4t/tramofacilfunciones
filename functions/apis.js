const request = require('request');
const environment = require('./environments/environment');
const firebase = require("firebase");
require("firebase/firestore");


/********************* credenciales firebase *********************/
firebase.initializeApp({

    apiKey: environment.credencialesFireBase.apiKey,
    authDomain: environment.credencialesFireBase.authDomain,
    projectId: environment.credencialesFireBase.projectId

});

/********************* getDataDB *********************/
function getDataDB() {

    var db = firebase.firestore();
    var respuesta = new Array();
    var citiesRef = db.collection("parametro")
    var query1 = citiesRef;

    var datos = {
        id: "",
        url: "",
        clientIdToken: "",
        clientSecretToken: "",
        correo: "",
        pass: "",
        host: ""
    };

    return new Promise(function(resolve, reject) {

        query1.get().then(function(querySnapshot) {

                querySnapshot.forEach(function(doc) {

                    const post = doc.data() || {};

                    datos = {
                        url: post.urlApi,
                        id: post.id,
                        clientIdToken: post.clientIdToken,
                        clientSecretToken: post.clientSecretToken,
                        correo: post.correo,
                        pass: post.contraseña,
                        host: post.host
                    }

                    respuesta.push(datos);

                });

                resolve(respuesta);

            })
            .catch(function(error) {

                reject(error);

            });

    })

}


/********************* obtener token*********************/
function funcionObtenerToken(value) {

    //console.log('url ' + JSON.stringify(value.url));
    //console.log('clientIdToken ' + JSON.stringify(value.clientIdToken));
    //console.log('clientSecretToken ' + JSON.stringify(value.clientSecretToken));

    var options = {
        'method': 'POST',
        'url': value.url,
        'timeout': 40000,
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
            'client_id': value.clientIdToken,
            'client_secret': value.clientSecretToken
        }
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                //console.log(' reject: ' + error);
                reject(error);
            } else {
                //console.log(' resolve: ' + JSON.parse(response.body).access_token);
                resolve(JSON.parse(response.body).access_token);
            }
        });
    })
}

/********************* enviar correo*********************/
function funcionEnvioCorreo(value) {
    var options = {
        'method': 'POST',
        'url': value.url,
        'timeout': 40000,
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "Envelope": { "Header": { "Body": { "getMessageRequest": { "system": "COBR", "from": "noresponder@cajalosandes.cl", "to": value.email, "cc": "mail_copia@cajalosandes.com", "typeMessage": "SOL_ACTUALIZAR_ESTADO", "subject": "COBRAR", "message": "nombreUsuario=" + value.nombre } } } } })
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                //console.log(' reject: ' + error);
                reject(error);
            } else {
                //console.log(' resolve: ' + JSON.parse(response.body).access_token);
                resolve(JSON.parse(response.body).access_token);
            }
        });
    })
}


/********************* api obtEmpresaPorRut *********************/
function obtEmpresaPorRut(value) {

    var token = 'Bearer ' + value.token;
    //console.log(' token generado: ' + token);

    var options = {
        'method': 'POST',
        'url': value.url,
        'timeout': 40000,
        'headers': {
            'Authorization': token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "rutPersona": value.rutPersona })
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

/********************* api obtEstadoDeclaracion *********************/
function obtEstadoDeclaracion(value) {

    var token = 'Bearer ' + value.token;
    //console.log(' token generado: ' + token);

    var options = {
        'method': 'POST',
        'url': value.url,
        'timeout': 40000,
        'headers': {
            'Authorization': token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

/********************* function obtEmpresaPorRut *********************/
async function functionObtEmpresaPorRut(value) {

    const DataDB = await getDataDB();
    const credToken = DataDB.find(data => data.id === '05');
    const credEmpresaPorRut = DataDB.find(data => data.id === '03');
    const credEstadoDeclaracion = DataDB.find(data => data.id === '01');

    const token = await funcionObtenerToken(credToken);

    value["token"] = token;
    value["url"] = credEmpresaPorRut.url;
    const empresaPorRut = await obtEmpresaPorRut(value);

    value["url"] = credEstadoDeclaracion.url;
    const estadoDeclaracion = await obtEstadoDeclaracion(value);

    var res = [JSON.parse(empresaPorRut), JSON.parse(estadoDeclaracion)];
    return res;
}


/********************* api actualizarRenta *********************/
function actualizarRenta(value) {

    var token = 'Bearer ' + value.token;
    //console.log(' token generado: ' + token);

    var options = {
        'method': 'POST',
        'url': value.url,
        'timeout': 40000,
        'headers': {
            'Authorization': token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "rutPersona": value.rutPersona, "idTramo": value.tipoContrato, "promedioDevengado": value.promedioDevengado, "listaRentas": value.detalleRenta })
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

/********************* function Actualizar Renta *********************/
async function functionActualizarRenta(value) {
    const DataDB = await getDataDB();
    const credToken = DataDB.find(data => data.id === '05');
    const credActualizarRenta = DataDB.find(data => data.id === '04');
    const credCorreo = DataDB.find(data => data.id === '07');

    const token = await funcionObtenerToken(credToken);

    value["token"] = token;
    value["url"] = credActualizarRenta.url;
    const actRenta = await actualizarRenta(value);

    /********************* enviar correo *********************/
    var respuestaActRenta = JSON.parse(actRenta);
    var keyNames = Object.keys(respuestaActRenta);
    if (keyNames.toString().toLocaleLowerCase().trim() != 'fault' || keyNames.toString().toLocaleLowerCase().trim() != 'envelope') {

        var codigoActRenta = respuestaActRenta.EntActualizarRentasPorRutPerOutEBM.codigo;
        if (codigoActRenta === 0) {
            value["url"] = credCorreo.url;
            const envioCorreo = await funcionEnvioCorreo(value);
            console.log('correo enviado');
        }
    }

    return respuestaActRenta;
}


/********************* api obtDetalleRenta *********************/
function obtDetalleRenta(value) {

    var token = 'Bearer ' + value.token;
    //console.log(' token generado: ' + token);

    var options = {
        'method': 'POST',
        'url': value.url,
        'timeout': 40000,
        'headers': {
            'Authorization': token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "rutPersona": value.rutPersona, "periodo": value.periodo, "tipoContrato": value.tipoContrato })
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    })
}

/********************* function obtDetalleRenta *********************/
async function functionObtDetalleRenta(value) {

    const DataDB = await getDataDB();
    const credToken = DataDB.find(data => data.id === '05');
    const credDetalleRenta = DataDB.find(data => data.id === '02');

    const token = await funcionObtenerToken(credToken);

    value["token"] = token;
    value["url"] = credDetalleRenta.url;
    const detalleRenta = await obtDetalleRenta(value);

    return JSON.parse(detalleRenta);
}


/********************* obtUsers *********************/
function obtUsers(value) {

    var token = value;
    console.log(' token: ' + token);

    var options = {
        'method': 'POST',
        'url': environment.obtUsers.url,
        'timeout': 40000,
        'headers': {
            'Content-Type': 'application/json',
            'Cookie': '__cfduid=d2feeab28b84b2810e0c188fa6bc3a4fc1587666860'
        },
        body: JSON.stringify({ "name": "Atta", "job": "Freelance Developer" })
    };

    return new Promise(function(resolve, reject) {

        request(options, function(error, response) {
            if (error) {
                //console.log(' obtUsers reject: ' + error);
                reject(error);
            } else {
                //console.log(' obtUsers resolve: ' + response.body);
                resolve(response.body);
            }
        });
    })
}

/********************* test *********************/
async function functionObtUsers() {
    const token = await funcionObtenerToken();
    const users = await obtUsers(token);

    console.log(' functionObtUsers: ' + users)
    return JSON.parse(Users);
}


/********************* exports *********************/
module.exports = {

    functionObtUsers,
    functionObtEmpresaPorRut,
    functionObtDetalleRenta,
    functionActualizarRenta

}