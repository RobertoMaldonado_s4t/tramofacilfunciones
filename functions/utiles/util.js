const moment = require('moment-timezone');


function number(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}


function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

function getFecha() {

    var fecha = moment().tz("America/Santiago").format('DD-MM-YYYY');
    return fecha;
}

function getAnio() {

    var anio = moment().tz("America/Santiago").format('YYYY');
    return anio;
}

function getDigitoVerificador(rut) {

    if (!rut || !rut.length || typeof rut !== 'string') {
        return -1;
    }

    var secuencia = [2, 3, 4, 5, 6, 7, 2, 3];
    var sum = 0;

    for (var i = rut.length - 1; i >= 0; i--) {
        var d = rut.charAt(i)
        sum += new Number(d) * secuencia[rut.length - (i + 1)];
    };

    var rest = 11 - (sum % 11);

    return rest === 11 ? 0 : rest === 10 ? "K" : rest;
}

function getDescEstado(value) {

    if (value == null) {
        return null;
    }

    var codigo = null;
    var texto = null;
    var titulo = null;


    switch (value) {
        case 0:
            titulo = 'NO TIENE DECLARACIONES';
            texto = 'Para esta solicitud no existen declaraciones.';
            break;
        case 1:
            titulo = 'SU SOLICITUD ESTÁ EN REVISIÓN';
            texto = 'Dentro de los próximos 5 días hábiles a contar de la fecha de ingreso le notificaremos vía mail el resultado de su solicitud.';
            break;
        case 2:
            titulo = 'SU SOLICITUD FUE APROBADA';
            texto = 'Su solicitud para el periodo actual fue validad y aprobada.';
            break;
        case 3:
            titulo = 'SU SOLICITUD FUE RECHAZADA';
            texto = 'Su solicitud para el periodo actual fue rechazada, por favor enviar su nueva solicitud junto a la documentación al siguiente correo: ';
            break;
        default:
            titulo = '';
            texto = '';
            codigo = '';
    }

    return { codigo: value, titulo: titulo, texto: texto };
}

function getMes(value) {

    if (value == null) {
        return null;
    }

    var estado = null;

    switch (value) {
        case 1:
            estado = 'Enero';
            break;
        case 2:
            estado = 'Febrero';
            break;
        case 3:
            estado = 'Marzo';
            break;
        case 4:
            estado = 'Abril';
            break;
        case 5:
            estado = 'Mayo';
            break;
        case 6:
            estado = 'Junio';
            break;
        case 7:
            estado = 'Julio';
            break;
        case 8:
            estado = 'Agosto';
            break;
        case 9:
            estado = 'Septiembre';
            break;
        case 10:
            estado = 'Octubre';
            break;
        case 11:
            estado = 'Noviembre';
            break;
        case 12:
            estado = 'Diciembre';
            break;
        default:
            estado = '';
    }

    return estado;
}


function getRentasTexto(tipoContrato, promedio) {

    var cadenaTexto1 = null;
    var cadenaTexto2 = null;
    var cadenaTexto3 = null;

    if (tipoContrato === 1) {

        cadenaTexto1 = 'declaro NO HABER PERCIBIDO INGRESOS durante ';
        cadenaTexto2 = 'el periodo de Enero a Junio de ' + getAnio();
        cadenaTexto3 = ', y el ingreso devengado para la asignacion familiar es de: ';

        if (promedio > 0) {
            cadenaTexto1 = 'Declaro: que mi ingreso promedio es de ' + number(promedio) + ' ';
            cadenaTexto2 = 'de Enero a Junio de ' + getAnio();
            cadenaTexto3 = '.';
        }

    } else {

        cadenaTexto1 = 'declaro NO HABER PERCIBIDO INGRESOS durante ';
        cadenaTexto2 = 'el periodo de Julio de ' + getAnio() - 1 + ' a Junio de ' + getAnio();
        cadenaTexto3 = ', y el ingreso devengado para la asignacion familiar es de: ';

        if (promedio > 0) {
            cadenaTexto1 = 'Declaro: que mi ingreso promedio es de ' + number(promedio) + ' ';
            cadenaTexto2 = 'de Julio a Junio de ' + getAnio();
            cadenaTexto3 = '.';
        }

    }

    var texto = { 'cadenaTexto1': cadenaTexto1, 'cadenaTexto2': cadenaTexto2, 'cadenaTexto3': cadenaTexto3 };

    return texto;

}

function getTemplateHtml(value) {
    const templateHtml = `<html>
    <head>
        <title>Cambio Situaci&oacute;n Laboral</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <style>
            .body {
                width: 595px;
                height: 989px;
                background-color: #ffffff;
                margin: auto;
            }
        </style>
    </head>
    
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <div class="body">
            <div style="padding: 12px;">
                <img src="cid:logo" alt="cambio situacion laboral" width="146" height="42" style="display:block">
            </div>
            <div>
                <img src="cid:imagen" alt="Cambio situacion laboral" width="477" height="419" style="margin-left: auto; display:block">
            </div>
            <div>
                <FONT FACE="sans-serif" COLOR="#646466">
                    <div style="text-align: center;font-size: 18px;font-weight: 300;line-height: 23px; padding-left: 80px; margin: auto;">
                        <strong>&iexcl;Hola ${value}&#33;</strong>
                    </div>
                </FONT>
            </div>
            <div>
                <FONT FACE="sans-serif" COLOR="#646466">
                    <div style="padding-left: 80px;">
                        <div style="width: 435px; font-size: 16px; text-align: center; font-weight: 300; display: block;  margin-top:30px;">
                            <p>Hemos registrado tu solicitud de actualización de tramo 2020.</p>
                        </div>
                        <div>
                            <p>Puedes revisar el estado de tu solicitud en Mi Sucursal -> Asignación Familiar -> Actualización de tramo.</p>
                        </div>
                        <br>
                        <div style="width: 100%; margin-top: 48px; display: block; margin-left: -12px;">
                            <div style="font-size: 16px; font-weight: 600; line-height: 10px;">
                                <p>Te deseamos un buen d&iacute;a,</p>
                                <p> Caja Los Andes</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <strong style="font-size: 16px; text-align: center; font-weight: 300; display: block; margin-top:37px; padding-left: 60px;">Si tienes alguna duda o consulta ingresa a Mi Sucursal o llama a nuestro Call Center al 600 510 00 00</strong>
                        <strong style="font-size: 12px;text-align: center;display:block;margin-top:37px;font-weight: 300;width: 493px;padding-left: 60px;">Las Cajas de Compensaci&oacute;n son fiscalizadas por la Superintendencia de Seguridad Social (www.suceso.cl)</strong>
                    </div>
                </FONT>
            </div>
        </div>
    </body>
    
    </html>`;

    return templateHtml;
}

function getDescEstadoRenta(value) {

    if (value == null) {
        return null;
    }

    var codigo = null;
    var desc = null;

    if (value.trim().toUpperCase() == 'S') {

        codigo = true;
        desc = 'Si tiene rentas';

    } else {

        codigo = false;
        desc = 'No tiene rentas';

    }

    return { codigo: codigo, desc: desc };
}

module.exports = {

    getDigitoVerificador,
    getFecha,
    getAnio,
    getDescEstado,
    getMes,
    getRentasTexto,
    getTemplateHtml,
    getDescEstadoRenta

}