'use strict';
const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
const desencriptar = require('./desencriptar');
const apis = require('./apis');
const util = require('./utiles/util');
const environment = require('./environments/environment');
const nodemailer = require('nodemailer');
const fs = require('fs');

/*
if (process.env.NODE_ENV != "production") {
    require("dotenv").config();
}
*/

var listaContratos = [
    { descripcion: "Indefinido", valor: 1 },
    { descripcion: "Plazo Fijo", valor: 2 }
];


/********************* rentas *********************/
var anio = 2020;

/********************* funcion obtener afiliado *********************/
exports.obtDetalleAfiliado = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        const { body } = req;
        const isValidMessage = body.parametro;


        if (!isValidMessage) {

            return res.send({
                codigo: 400,
                mensaje: 'Ejecución fallida',
                payload: null
            });
        }

        try {

            /********************* Desencriptar Informacion *********************/
            var desencriptarDatosEntrada = desencriptar.desencriptarInformacion(body.parametro);
            var rutEntrada = desencriptarDatosEntrada.usuario.rut;
            var nombreEntrada = desencriptarDatosEntrada.usuario.nombre;
            var emailEntrada = desencriptarDatosEntrada.usuario.email;

            /********************* Datos Afiliado *********************/
            var rutAfiliado = rutEntrada + '-' + util.getDigitoVerificador(rutEntrada);
            var afiliado = { nombre: nombreEntrada, rut: rutAfiliado, email: emailEntrada };

            /********************* Fecha Asignacion *********************/
            var asigFecha = util.getFecha();

            /********************* Año Asignacion *********************/
            var anio = util.getAnio();

            /********************* servico ObtEmpresaPorRut *********************/
            console.log("------------- ObtEmpresaPorRut -------------");

            var parametro = { 'rutPersona': parseInt(rutEntrada) };
            console.log("parametro: " + JSON.stringify(parametro));

            apis.functionObtEmpresaPorRut(parametro).then(function(result) {

                console.log(' ejecucion en index: ' + JSON.stringify(result));

                /********************* validar api *********************/
                var keyNames = Object.keys(result);
                if (keyNames.toString().toLocaleLowerCase().trim() == 'fault' || keyNames.toString().toLocaleLowerCase().trim() == 'envelope') {
                    return res.send({
                        codigo: 500,
                        mensaje: 'Api not found',
                        payload: null
                    });
                }

                var valueEstado = result[1].EntObtenerEstadoPorRutPerOutEBM.estado;
                var estadoSolicitud = util.getDescEstado(valueEstado);
                var empresas = result[0].EntListarEmpresasByPer_RutOutEBM.listaEmpresas;
                var informacion = { afiliado, empresas, listaContratos, anio, asigFecha, estadoSolicitud };

                return res.send({
                    codigo: 200,
                    mensaje: 'Ejecución exitosa',
                    payload: informacion
                });

            }, function(err) {

                console.log(err);
                return res.send({
                    codigo: 500,
                    mensaje: 'Ejecución fallida',
                    payload: null
                });

            })


        } catch (err) {

            console.log(err);
            return res.send({
                codigo: 500,
                mensaje: 'Ejecución fallida',
                payload: null
            });

        }

    });
});



/********************* funcion obtener Detalle Renta *********************/
exports.obtDetalleRenta = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        const { body } = req;
        const isValidMessage = body.rut && body.tipoContrato;


        if (!isValidMessage) {

            return res.send({
                codigo: 400,
                mensaje: 'Ejecución fallida',
                payload: null
            });
        }

        try {

            /********************* servico obtDetalleRenta *********************/
            console.log("------------- obtDetalleRenta -------------");

            var parametro = { 'rutPersona': parseInt(body.rut), 'periodo': 2019, 'tipoContrato': parseInt(body.tipoContrato) };
            console.log("parametro: " + parametro);

            /********************* Fecha Asignacion *********************/
            var asigFecha = util.getFecha();

            /********************* Año Asignacion *********************/
            var anio = util.getAnio();

            apis.functionObtDetalleRenta(parametro).then(function(result) {

                console.log(' ejecucion en index: ' + JSON.stringify(result));

                /********************* validar api *********************/
                var keyNames = Object.keys(result);
                if (keyNames.toString().toLocaleLowerCase().trim() == 'fault' || keyNames.toString().toLocaleLowerCase().trim() == 'envelope') {
                    return res.send({
                        codigo: 500,
                        mensaje: 'Api not found',
                        payload: null
                    });
                }

                //sacar ini
                var renta = [
                    { periodo: 202006, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtro: 0, rentasIndependiente: 0, rentasSubsidio: 350000, rentasPension: 0, total: 1300000, estadoRenta: 1 },
                    { periodo: 202005, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 500000, rentasPension: 0, total: 1450000, estadoRenta: 1 },
                    { periodo: 202004, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 200000, rentasSubsidio: 0, rentasPension: 0, total: 1150000, estadoRenta: 1 },
                    { periodo: 202003, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 150000, rentasIndependiente: 0, rentasSubsidio: 0, rentasPension: 0, total: 1100000, estadoRenta: 1 },
                    { periodo: 202002, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 0, rentasPension: 0, total: 950000, estadoRenta: 1 },
                    { periodo: 202001, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 0, rentasPension: 600000, total: 1550000, estadoRenta: 1 },
                    { periodo: 201912, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 350000, rentasPension: 0, total: 1300000, estadoRenta: 1 },
                    { periodo: 201911, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 500000, rentasPension: 0, total: 1450000, estadoRenta: 1 },
                    { periodo: 201910, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 200000, rentasSubsidio: 0, rentasPension: 0, total: 1150000, estadoRenta: 1 },
                    { periodo: 201909, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 150000, rentasIndependiente: 0, rentasSubsidio: 0, rentasPension: 0, total: 1100000, estadoRenta: 1 },
                    { periodo: 201908, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 0, rentasPension: 0, total: 950000, estadoRenta: 1 },
                    { periodo: 201907, rutEmpleador: 70285100, rentasEmpleador: 950000, rentasOtras: 0, rentasIndependiente: 0, rentasSubsidio: 0, rentasPension: 600000, total: 1550000, estadoRenta: 1 }
                ];

                var detalle = { renta };
                //sacar fin

                //var detalle = result.EntObtenerRentasPorRutPerOutEBM.listaRentas;


                var total = 0;
                var contador = 0;
                for (const index in detalle.renta) {
                    total = detalle.renta[index].total + total;
                    contador = contador + 1;
                }

                var promedio = total / contador;
                var mensaje = util.getRentasTexto(parseInt(body.tipoContrato), Math.trunc(promedio));
                var rentasPeriodo = result.EntObtenerRentasPorRutPerOutEBM.rentasPeriodo;

                var asigFamiliar = Math.trunc(promedio);
                var estadoRenta = util.getDescEstadoRenta(rentasPeriodo);

                var detalleRenta = { anio, asigFecha, asigFamiliar, detalle, mensaje, estadoRenta };

                return res.send({
                    codigo: 200,
                    mensaje: 'Ejecución exitosa',
                    payload: detalleRenta
                });

            }, function(err) {

                console.log(err);
                return res.send({
                    codigo: 500,
                    mensaje: 'Ejecución fallida',
                    payload: null
                });

            })


        } catch (err) {

            console.log(err);
            return res.send({
                codigo: 500,
                mensaje: 'Ejecución fallida',
                payload: null
            });

        }

    });
});

/********************* nodemailer *********************/
const transporter = nodemailer.createTransport({
    service: environment.credencialesCorreo.service,
    auth: {
        user: environment.credencialesCorreo.user,
        pass: environment.credencialesCorreo.pass
    }
});

/********************* funcion Actualizar Renta *********************/
exports.ActualizarRenta = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        const { body } = req;
        const isValidMessage = body.rutPersona && body.nombreUsuario && body.email && body.tipoContrato && body.detalleRenta;

        if (!isValidMessage && body.devengado >= 0) {

            return res.send({
                codigo: 400,
                mensaje: 'Ejecución fallida',
                payload: null
            });
        }

        try {

            /********************* servico ActualizarRenta *********************/
            console.log("------------- ActualizarRenta -------------");

            var parametro = { 'rutPersona': parseInt(body.rut), 'tipoContrato': parseInt(body.tipoContrato), 'promedioDevengado': body.devengado, 'detalleRenta': body.detalleRenta, 'correo': body.correo, 'nombre': body.nombreUsuario };
            console.log("parametro: " + parametro);

            apis.functionActualizarRenta(parametro).then(function(result) {

                console.log(' ejecucion en index: ' + JSON.stringify(result));

                /********************* validar api *********************/
                var keyNames = Object.keys(result);
                if (keyNames.toString().toLocaleLowerCase().trim() == 'fault' || keyNames.toString().toLocaleLowerCase().trim() == 'envelope') {

                    return res.send({
                        codigo: 500,
                        mensaje: 'Api not found',
                        payload: null
                    });

                }

                /********************* validar respuesta api *********************/
                var codigoActRenta = result.EntActualizarRentasPorRutPerOutEBM.codigo;
                if (codigoActRenta != 0) {
                    return res.send({
                        codigo: 500,
                        mensaje: 'Favor de reintentar nuevamente',
                        payload: null
                    });
                }

                var respActRenta = result.EntActualizarRentasPorRutPerOutEBM;

                return res.send({
                    codigo: 200,
                    mensaje: 'Ejecución exitosa',
                    payload: respActRenta
                });

            }, function(err) {

                console.log(err);
                return res.send({
                    codigo: 500,
                    mensaje: 'Ejecución fallida',
                    payload: null
                });
            })

        } catch (err) {

            console.log(err);
            return res.send({
                codigo: 500,
                mensaje: 'Ejecución fallida',
                payload: null
            });

        }

    });
});