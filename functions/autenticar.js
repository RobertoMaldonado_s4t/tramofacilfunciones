const jwt = require('jsonwebtoken');

function obtenerToken(value) {

    if (!value) {
        return null;
    }

    var tokenData = {
        username: value
    };

    var token = jwt.sign(tokenData, 'Secret Password', {
        expiresIn: 60 * 1 // 1 min
    });

    return token;
}

function verificarToken(value) {

    //var token = req.headers['authorization']

    var resp = false;

    if (!value) {
        return resp;
    }

    var token = value;
    token = token.replace('Bearer ', '');

    jwt.verify(token, 'Secret Password', function(err, user) {
        if (err) {
            resp = false;
        } else {
            resp = true;
        }
    })

    return resp;

}

module.exports = {
    "obtenerToken": obtenerToken,
    "verificarToken": verificarToken,
}